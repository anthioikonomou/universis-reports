import unirest from 'unirest';
import {HttpError} from "@themost/common";
import {ApplicationService} from "@themost/common";

const printSettingsProperty = Symbol('printSettings');

/**
 * @class
 * @property {string} server
 * @property {string} user
 * @property {string} password
 */
class ConfigurationPrintSettings {
    /**
     * @constructor
     */
    constructor() {
        //
    }
}

export class PrintService extends ApplicationService {
    /**
     * @param {IApplication} app
     */
    constructor(app) {
        super(app);
        /**
         *
         * @type {ConfigurationPrintSettings}
         */
        this[printSettingsProperty] = Object.assign(app.getConfiguration().getSourceAt('settings/print'),
            new ConfigurationPrintSettings());

        /**
         * @name PrintService#settings
         * @type ConfigurationPrintSettings
         */

        Object.defineProperty(this, 'settings', {
            get:()=> {
                return this[printSettingsProperty];
            }
        })

    }

    /**
     *
     * @param {string} report
     * @param {string} format
     */
    resolve(report, format) {
        let finalReport;
        if (/^\//g.test(report)) {
            finalReport = "/reports/".concat(report);
        }
        else {
            finalReport = report;
        }
        return new URL(finalReport, this.settings.server).toString().concat(/^\./.test(format) ? format : '.' + format);
    }

    /**
     * @param {string} report
     * @param {string} format
     * @param {*} params
     * @returns Promise<Buffer>
     * @example
     * const service = context.getApplication().getService(PrintService);
     * service.print('reports/Student Certificate','pdf', {
     *      "id": 54
     * }).then((result) => {
     *      fs.writeFile(path.resolve(process.env.HOME || process.env.USERPROFILE,'./Reports/StudentCertificate.pdf'), result, "binary", (err) => {
     *              return done(err);
     *      });
     * }
     */
    print(report, format, params) {
        const self = this;
        return new Promise((resolve, reject) => {
            if (self.settings.server==null) {
                return reject(new Error('Report server cannot be empty at this context.'));
            }
            if (self.settings.user == null || this.settings.password==null) {
                return reject(new Error('Report server security settings are missing or cannot be found.'));
            }
            /**
             * @type {string}
             * @example http://reports.example.com/jasperserver/rest_v2/reports/reports/Student Certificate.pdf
             * where server: http://reports.example.com/jasperserver/,
             * report: reports/Student Certificate and format:pdf
             */
            unirest.get(self.resolve(report, format))
                .encoding(null)
                .auth({
                    user: this.settings.user,
                    pass: this.settings.password,
                    sendImmediately: true
                }).query(params).end(function (response) {
                if (response.statusCode !== 200) {
                    return reject(new HttpError(response.statusCode));
                }
                return resolve(response.body);
            });
        });
    }

}
